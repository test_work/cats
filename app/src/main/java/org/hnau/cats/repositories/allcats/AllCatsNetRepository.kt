package org.hnau.cats.repositories.allcats

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.hnau.cats.domain.data.Cat
import org.hnau.cats.domain.repositories.AllCatsRepository
import org.hnau.cats.utils.BehaviorSubject
import org.hnau.cats.utils.actualValue
import org.hnau.cats.utils.collapse
import org.hnau.cats.utils.deferredloader.DeferredLoader
import org.hnau.cats.utils.deferredloader.LoadingResult
import org.hnau.cats.utils.optional.valueOrElse
import org.hnau.cats.utils.setUnique
import javax.inject.Inject


class AllCatsNetRepository @Inject constructor() : AllCatsRepository {

    companion object {

        private const val pageSize = 9

    }

    private val api = GetAllCatsRetrofitApi.get()

    private val isErrorInner = BehaviorSubject(false)

    override val isError: Observable<Boolean> by ::isErrorInner

    private var loadLastPageAfterError: (() -> Unit)? = null

    private fun loadCats(
        page: Int
    ): Single<LoadingResult<Int, NetCat>> = api
        .getCatsPage(
            limit = pageSize,
            page = page
        )
        .subscribeOn(Schedulers.io())
        .doOnSubscribe {
            isErrorInner::actualValue.setUnique(false)
        }
        .map { cats ->
            LoadingResult(
                items = cats,
                nextKey = page + 1
            )
        }
        .onErrorResumeNext {
            isErrorInner::actualValue.setUnique(true)
            Single
                .create<Single<LoadingResult<Int, NetCat>>> { emitter ->
                    loadLastPageAfterError = {
                        emitter.onSuccess(loadCats(page))
                    }
                }
                .flatMap { it }
        }

    private fun createNewAllCatsDeferredLoader() = DeferredLoader<Int, NetCat>(
        load = { key ->
            val page = key.valueOrElse { 0 }
            loadCats(page)
        },
        distanceBeforeLastItemToLoadNextPage = pageSize / 2
    )

    private val catsListLoader =
        BehaviorSubject(createNewAllCatsDeferredLoader())

    private val allCats = DeferredLoader(
        items = catsListLoader
            .map { catsLoader ->
                catsLoader.items.map { cats ->
                    cats.map(NetCat::toCat)
                }
            }
            .collapse(),
        onItemWasUsed = { index ->
            catsListLoader.actualValue.onItemWasUsed(index)
        }
    )

    override fun getAllCats(): DeferredLoader<Cat> = allCats

    override fun reloadAll() {
        catsListLoader.actualValue = createNewAllCatsDeferredLoader()
    }

    override fun reloadAfterError() {
        val loadLastPageAfterError = loadLastPageAfterError ?: return
        this.loadLastPageAfterError = null
        loadLastPageAfterError()
    }

}
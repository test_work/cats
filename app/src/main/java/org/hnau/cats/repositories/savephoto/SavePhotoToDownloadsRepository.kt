package org.hnau.cats.repositories.savephoto

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import org.hnau.cats.R
import org.hnau.cats.domain.repositories.SavePhotoRepository
import org.hnau.cats.utils.ToastShower
import org.hnau.cats.utils.permissions.PermissionToRequest
import org.hnau.cats.utils.permissions.PermissionsRequester
import javax.inject.Inject


class SavePhotoToDownloadsRepository @Inject constructor(
    private val context: Context,
    private val permissionsRequester: PermissionsRequester,
    private val toastShower: ToastShower
) : SavePhotoRepository {

    private val downloadsManager =
        context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

    private fun savePhotoNow(
        url: String
    ) {
        val uri = Uri.parse(url)
        val filename = uri.lastPathSegment ?: error("Unable to extract file name from $url")
        downloadsManager.enqueue(
            DownloadManager.Request(uri).apply {
                setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename)
                allowScanningByMediaScanner()
                setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                setDescription(context.getString(R.string.save_cat_photo_to_downloads_notification_title))
            }
        )
        toastShower.showToast(R.string.save_cat_photo_to_downloads_started_message)
    }

    @SuppressLint("CheckResult")
    override fun savePhoto(
        url: String
    ) {
        permissionsRequester
            .requestPermission(PermissionToRequest.writeToExternalStorage)
            .andThen { emitter ->
                try {
                    savePhotoNow(url)
                    emitter.onComplete()
                } catch (th: Throwable) {
                    emitter.onError(th)
                }
            }
            .onErrorComplete()
            .subscribe { }
    }

}
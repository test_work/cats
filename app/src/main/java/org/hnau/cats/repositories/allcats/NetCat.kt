package org.hnau.cats.repositories.allcats

import androidx.annotation.Keep
import org.hnau.cats.domain.data.Cat


@Keep
data class NetCat(
    val id: String,
    val url: String
) {

    fun toCat() = Cat(
        id = id,
        photoUrl = url
    )

}
package org.hnau.cats.repositories.favoritecats

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Observable


@Dao
interface FavoriteCatsDao {

    @Query("SELECT * FROM cat")
    fun getAll(): Observable<List<DBCat>>

    @Insert
    fun add(cat: DBCat)

    @Query("DELETE FROM cat WHERE id = :id")
    fun remove(id: String)


}
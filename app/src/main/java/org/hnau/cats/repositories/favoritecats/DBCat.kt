package org.hnau.cats.repositories.favoritecats

import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.hnau.cats.domain.data.Cat


@Keep
@Entity(tableName = "cat")
data class DBCat(
    @PrimaryKey
    val id: String = "",
    val photoUrl: String = ""
) {

    companion object {

        fun fromCat(
            cat: Cat
        ) = DBCat(
            id = cat.id,
            photoUrl = cat.photoUrl
        )

    }

    fun toCat() = Cat(
        id = id,
        photoUrl = photoUrl
    )

}
package org.hnau.cats.repositories.favoritecats

import android.content.Context
import io.reactivex.Observable
import org.hnau.cats.domain.data.Cat
import org.hnau.cats.domain.data.CatId
import org.hnau.cats.domain.repositories.FavoriteCatsRepository
import org.hnau.cats.utils.Executors
import javax.inject.Inject


class FavoriteCatsDBRepository @Inject constructor(
    context: Context,
    private val executors: Executors
) : FavoriteCatsRepository {

    private val favoriteCatsDao =
        CatsDatabase.create(context).favoriteCatsDao()

    override fun getFavoriteCats(): Observable<List<Cat>> = favoriteCatsDao
        .getAll()
        .map { dbCats ->
            dbCats.map(DBCat::toCat)
        }

    private inline fun executeInIO(
        crossinline block: () -> Unit
    ) = executors.io.execute {
        block()
    }

    override fun addCatToFavorites(
        cat: Cat
    ) = executeInIO {
        favoriteCatsDao.add(DBCat.fromCat(cat))
    }

    override fun removeCatFromFavorites(
        catId: CatId
    ) = executeInIO {
        favoriteCatsDao.remove(catId)
    }

}
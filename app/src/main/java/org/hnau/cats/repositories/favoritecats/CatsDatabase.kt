package org.hnau.cats.repositories.favoritecats

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [DBCat::class], version = 1)
abstract class CatsDatabase : RoomDatabase() {

    companion object {

        fun create(
            context: Context
        ) = Room
            .databaseBuilder(
                context,
                CatsDatabase::class.java,
                "cats_database"
            )
            .build()

    }

    abstract fun favoriteCatsDao(): FavoriteCatsDao

}
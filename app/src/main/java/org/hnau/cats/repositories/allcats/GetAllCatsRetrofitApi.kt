package org.hnau.cats.repositories.allcats

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface GetAllCatsRetrofitApi {

    companion object {

        fun get(): GetAllCatsRetrofitApi = Retrofit
            .Builder()
            .baseUrl("https://api.thecatapi.com/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(GetAllCatsRetrofitApi::class.java)

    }

    @GET("images/search?order=Desc")
    fun getCatsPage(
        @Query("limit") limit: Int,
        @Query("page") page: Int
    ): Single<List<NetCat>>

}
package org.hnau.cats.utils.deferredloader

import android.annotation.SuppressLint
import io.reactivex.Observable
import io.reactivex.Single
import org.hnau.cats.utils.BehaviorSubject
import org.hnau.cats.utils.actualValue
import org.hnau.cats.utils.optional.Optional


@Suppress("FunctionName")
fun <T> DeferredLoader(
    items: Observable<List<T>>,
    onItemWasUsed: (itemIndex: Int) -> Unit
) = object : DeferredLoader<T> {

    override val items = items

    override fun onItemWasUsed(
        itemIndex: Int
    ) = onItemWasUsed.invoke(
        itemIndex
    )

}

@Suppress("FunctionName")
fun <K, T> DeferredLoader(
    load: (key: Optional<K>) -> Single<LoadingResult<K, T>>,
    distanceBeforeLastItemToLoadNextPage: Int
): DeferredLoader<T> {

    var nextPageKey: Optional<K> = Optional.None
    val items = ArrayList<T>()

    var isLoading = false

    val itemsSubject =
        BehaviorSubject<List<T>>(items)


    fun onItemsChanged() {
        itemsSubject.actualValue = items
    }

    @SuppressLint("CheckResult")
    fun loadNextItems() {
        if (isLoading) {
            return
        }
        isLoading = true
        load(nextPageKey).subscribe { (newItems, nextKey) ->
            nextPageKey = Optional.Some(nextKey)
            items.addAll(newItems)
            isLoading = false
            onItemsChanged()
        }
    }

    @Synchronized
    fun loadInitialItemsIfNeed() {
        if (nextPageKey == Optional.None) {
            loadNextItems()
        }
    }

    @Synchronized
    fun onItemWasUsed(
        index: Int
    ) {
        if (index + distanceBeforeLastItemToLoadNextPage + 1 >= items.size) {
            loadNextItems()
        }
    }

    return DeferredLoader(
        items = itemsSubject
            .doOnSubscribe { loadInitialItemsIfNeed() }
            .replay(1)
            .autoConnect(),
        onItemWasUsed = ::onItemWasUsed
    )

}
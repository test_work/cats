package org.hnau.cats.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.LayoutRes
import kotlin.reflect.KMutableProperty0

inline fun <T> setUnique(
    oldValue: T,
    value: T,
    set: (T) -> Unit
) = (oldValue != value).also { isUnique ->
    if (isUnique) {
        set(value)
    }
}

fun <T> KMutableProperty0<T>.setUnique(value: T) =
    setUnique(get(), value, this::set)

fun Context.inflateView(
    @LayoutRes layoutResId: Int
): View = LayoutInflater
    .from(this)
    .inflate(layoutResId, null, false)

operator fun <A, B, C> ((A) -> B).plus(
    other: (B) -> C
): (A) -> C = { source ->
    other(this(source))
}
package org.hnau.cats.utils.view

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout


class SquareByWidthFrameLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(
    context,
    attrs,
    defStyleAttr
) {

    override fun onMeasure(
        widthMeasureSpec: Int,
        heightMeasureSpec: Int
    ) {
        val size = MeasureSpec.getSize(widthMeasureSpec)
        val measureSpec = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY)
        super.onMeasure(measureSpec, measureSpec)
    }

}
package org.hnau.cats.utils.permissions

import io.reactivex.Completable


interface PermissionsRequester {

    fun requestPermission(permissionToRequest: PermissionToRequest): Completable

}
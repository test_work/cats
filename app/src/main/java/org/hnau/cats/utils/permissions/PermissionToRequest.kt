package org.hnau.cats.utils.permissions

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.StringRes
import org.hnau.cats.R


data class PermissionToRequest(
    val androidName: String,
    @StringRes val descriptionResId: Int
) : Parcelable {

    companion object CREATOR : Parcelable.Creator<PermissionToRequest> {

        val writeToExternalStorage = PermissionToRequest(
            androidName = android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            descriptionResId = R.string.permission_explanation_write_external_storage
        )

        override fun createFromParcel(
            parcel: Parcel
        ) = PermissionToRequest(
            parcel = parcel
        )

        override fun newArray(
            size: Int
        ) = arrayOfNulls<PermissionToRequest>(
            size = size
        )

    }

    constructor(parcel: Parcel) : this(
        androidName = parcel.readString()!!,
        descriptionResId = parcel.readInt()
    )

    override fun writeToParcel(
        parcel: Parcel,
        flags: Int
    ) {
        parcel.writeString(androidName)
        parcel.writeInt(descriptionResId)
    }

    override fun describeContents() = 0

}
package org.hnau.cats.utils.view

import android.view.View
import io.reactivex.Observable
import org.hnau.cats.utils.*


val View.isAttachedToWindowObservable: Observable<Boolean>
    get() = BehaviorSubject(false).apply {

        val viewIsAttachedToWindowListener =
            object : View.OnAttachStateChangeListener {

                override fun onViewAttachedToWindow(v: View?) {
                    ::actualValue.setUnique(true)
                }

                override fun onViewDetachedFromWindow(v: View?) {
                    ::actualValue.setUnique(false)
                }

            }

        this@isAttachedToWindowObservable.addOnAttachStateChangeListener(
            viewIsAttachedToWindowListener
        )

    }

val View.lifecycleState
    get() = context.lifecycleOwner.lifecycle.state.useWhen(isAttachedToWindowObservable)
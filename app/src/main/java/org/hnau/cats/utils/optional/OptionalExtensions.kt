package org.hnau.cats.utils.optional


inline fun <I, O> Optional<I>.map(
        transform: (I) -> O
) = checkOptional(
        ifSome = { Optional.Some(transform(it)) },
        ifNone = { Optional.None }
)
package org.hnau.cats.utils

import android.content.DialogInterface
import androidx.fragment.app.DialogFragment
import io.reactivex.Completable
import io.reactivex.subjects.CompletableSubject


abstract class CompletableDialogFragment : DialogFragment() {

    private val finishedInner = CompletableSubject.create()

    val finished: Completable by ::finishedInner

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        finishedInner.onComplete()
    }

}
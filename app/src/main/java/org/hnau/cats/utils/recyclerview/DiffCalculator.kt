package org.hnau.cats.utils.recyclerview

import androidx.recyclerview.widget.DiffUtil


class DiffCalculator<T>(
    itemCallback: DiffUtil.ItemCallback<T>
) {

    private class Callback<T>(
        private val itemCallback: DiffUtil.ItemCallback<T>
    ) : DiffUtil.Callback() {

        private var oldListOrNull: List<T>? = null

        private val oldList
            get() = oldListOrNull ?: error("Old list was not initialized")

        private var newListOrNull: List<T>? = null

        private val newList
            get() = newListOrNull ?: error("New list was not initialized")

        fun fill(
            oldList: List<T>,
            newList: List<T>
        ) {
            this.oldListOrNull = oldList
            this.newListOrNull = newList
        }

        fun clear() {
            oldListOrNull = null
            newListOrNull = null
        }

        inline fun <R> use(
            oldList: List<T>,
            newList: List<T>,
            block: DiffUtil.Callback.() -> R
        ): R {
            fill(oldList, newList)
            val result = block()
            clear()
            return result
        }

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            itemCallback.areItemsTheSame(oldList[oldItemPosition], newList[newItemPosition])

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            itemCallback.areContentsTheSame(oldList[oldItemPosition], newList[newItemPosition])

    }

    private val callback = Callback(itemCallback)

    fun calcDiff(
        oldList: List<T>,
        newList: List<T>,
        detectMoves: Boolean = true
    ) = callback.use(oldList, newList) {
        DiffUtil.calculateDiff(this, detectMoves)
    }

}

fun <T> DiffUtil.ItemCallback<T>.toDiffCalculator() =
    DiffCalculator(this)
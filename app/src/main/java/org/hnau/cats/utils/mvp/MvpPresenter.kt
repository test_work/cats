package org.hnau.cats.utils.mvp


interface MvpPresenter<V> {

    fun onAttach(view: V)

    fun onDetach()

}
package org.hnau.cats.utils


object ShouldNeverHappenException : AssertionError("This should never happen")
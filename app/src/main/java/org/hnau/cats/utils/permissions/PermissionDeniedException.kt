package org.hnau.cats.utils.permissions


class PermissionDeniedException(
    val forever: Boolean
) : Exception()
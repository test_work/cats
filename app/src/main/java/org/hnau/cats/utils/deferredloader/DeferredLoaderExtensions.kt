package org.hnau.cats.utils.deferredloader


fun <I, O> DeferredLoader<I>.map(
    transform: (I) -> O
) = DeferredLoader(
    items = items.map { itemsList -> itemsList.map(transform) },
    onItemWasUsed = ::onItemWasUsed
)
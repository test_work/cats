package org.hnau.cats.utils.permissions

import android.app.Activity
import io.reactivex.Completable


data class RequestPermissionsContext(
    val activity: Activity,
    val showPermissionsExplanation: (PermissionToRequest) -> Completable,
    val showGoToSettingsDialog: () -> Unit
)
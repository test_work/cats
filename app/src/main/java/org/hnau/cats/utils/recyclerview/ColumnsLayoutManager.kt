package org.hnau.cats.utils.recyclerview

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager


class ColumnsLayoutManager(
    context: Context
) : GridLayoutManager(
    context,
    columnsCount,
    VERTICAL,
    false
) {

    companion object {

        const val columnsCount = 3

    }

}
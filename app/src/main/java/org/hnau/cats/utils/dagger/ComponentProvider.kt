package org.hnau.cats.utils.dagger

import org.hnau.cats.utils.optional.Optional


interface ComponentProvider<C : Any> {

    val component: Optional<C>

}
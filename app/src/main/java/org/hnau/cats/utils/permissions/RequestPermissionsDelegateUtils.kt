package org.hnau.cats.utils.permissions

import android.app.Activity
import android.os.Build
import androidx.annotation.RequiresApi
import io.reactivex.Completable
import org.hnau.cats.utils.AutoKeyStorage
import org.hnau.cats.utils.optional.Optional
import org.hnau.cats.utils.optional.checkOptional
import java.util.concurrent.CancellationException


object RequestPermissionsDelegateUtils {

    @RequiresApi(Build.VERSION_CODES.M)
    fun createRequestPermissionImmediateCompletable(
        callbacks: AutoKeyStorage<Int, (Optional<Boolean>) -> Unit>,
        permissionToRequest: PermissionToRequest,
        activity: Activity
    ) = Completable
        .create { callback ->

            val requestId = callbacks.insert { optionalResult ->
                optionalResult.checkOptional(
                    ifSome = { granted ->
                        if (granted) {
                            callback.onComplete()
                        } else {
                            callback.onError(
                                PermissionDeniedException(
                                    forever = !PermissionsUtils.checkNeedShowExplanation(
                                        activity = activity,
                                        permissionAndroidName = permissionToRequest.androidName
                                    )
                                )
                            )
                        }
                    },
                    ifNone = {
                        callback.onError(CancellationException())
                    }
                )

            }

            requestId.checkOptional(
                ifSome = { requestCode ->
                    activity.requestPermissions(
                        arrayOf(permissionToRequest.androidName),
                        requestCode
                    )
                },
                ifNone = {
                    callback.onError(CancellationException())
                }
            )

        }

}
package org.hnau.cats.utils.recyclerview

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


abstract class SingleOrEmptyAdapter<VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

    private var itemIsVisible = false

    fun setItemVisibility(
        itemIsVisible: Boolean
    ) {
        if (this.itemIsVisible == itemIsVisible) return
        this.itemIsVisible = itemIsVisible
        if (itemIsVisible) {
            notifyItemInserted(0)
        } else {
            notifyItemRemoved(0)
        }
    }

    override fun onBindViewHolder(holder: VH, position: Int) {}

    override fun getItemCount() =
        if (itemIsVisible) 1 else 0

}

@Suppress("FunctionName")
fun SingleOrEmptyAdapter(
    createView: () -> View
) = object : SingleOrEmptyAdapter<StatelessViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = StatelessViewHolder(
        createView()
    )

}
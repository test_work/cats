package org.hnau.cats.utils.permissions

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import org.hnau.cats.utils.ShouldNeverHappenException


object PermissionsUtils {

    fun checkPermissionState(
        activity: Activity,
        permissionAndroidName: String
    ) = ContextCompat.checkSelfPermission(
        activity,
        permissionAndroidName
    ).let { resultCode ->
        when (resultCode) {
            PackageManager.PERMISSION_GRANTED -> true
            PackageManager.PERMISSION_DENIED -> false
            else -> throw ShouldNeverHappenException
        }
    }

    fun checkNeedShowExplanation(
        activity: Activity,
        permissionAndroidName: String
    ) = ActivityCompat.shouldShowRequestPermissionRationale(
        activity,
        permissionAndroidName
    )

}
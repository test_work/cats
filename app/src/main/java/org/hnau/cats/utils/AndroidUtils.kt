package org.hnau.cats.utils

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import org.hnau.cats.R
import java.util.*


object AndroidUtils {

    private val random = Random(System.currentTimeMillis())

    fun generateId() = random.nextInt() and 0x0000ffff

}

inline fun FragmentManager.transaction(config: FragmentTransaction.() -> Unit) =
    beginTransaction().apply(config)

fun FragmentTransaction.animate() =
    setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
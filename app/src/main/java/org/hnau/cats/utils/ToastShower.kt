package org.hnau.cats.utils

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes
import javax.inject.Inject


class ToastShower @Inject constructor(
    private val context: Context
) {

    enum class Length(
        val key: Int
    ) {
        LONG(key = Toast.LENGTH_LONG),
        SHORT(key = Toast.LENGTH_SHORT)
    }

    fun showToast(
        @StringRes textResId: Int,
        length: Length = Length.SHORT
    ) = Toast
        .makeText(context, textResId, length.key)
        .show()

}
package org.hnau.cats.utils.recyclerview

import android.view.View
import androidx.recyclerview.widget.RecyclerView


class StatelessViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
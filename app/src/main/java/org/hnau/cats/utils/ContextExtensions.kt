package org.hnau.cats.utils

import android.content.Context
import android.view.ContextThemeWrapper
import androidx.lifecycle.LifecycleOwner


val Context.lifecycleOwner: LifecycleOwner
    get() = when (this) {
        is LifecycleOwner -> this
        is ContextThemeWrapper -> baseContext.lifecycleOwner
        else -> throw IllegalArgumentException("'${javaClass.name}' not implements android.arch.lifecycle.LifecycleOwner")
    }
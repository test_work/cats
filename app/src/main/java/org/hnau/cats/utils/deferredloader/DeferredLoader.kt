package org.hnau.cats.utils.deferredloader

import io.reactivex.Observable


interface DeferredLoader<T> {

    val items: Observable<List<T>>

    fun onItemWasUsed(itemIndex: Int)

}

data class LoadingResult<K, T>(
    val items: List<T>,
    val nextKey: K
)


package org.hnau.cats.utils

import android.content.Context
import androidx.core.content.ContextCompat
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class Executors(
    context: Context
) {

    val main: Executor = ContextCompat.getMainExecutor(context)

    val io: Executor = Executors.newCachedThreadPool()

}
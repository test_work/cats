package org.hnau.cats.utils.dagger


interface IndependentComponentProvider {

    fun fill()

    fun clear()

}
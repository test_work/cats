package org.hnau.cats.utils.optional


inline fun <T, R> Optional<T>.checkOptional(
    ifSome: (T) -> R,
    ifNone: () -> R
) = when (this) {
    Optional.None -> ifNone()
    is Optional.Some -> ifSome(value)
}

inline fun <T> Optional<T>.valueOrElse(
    ifNone: () -> T
) = checkOptional(
    ifSome = { it },
    ifNone = ifNone
)

val <T> Optional<T>.valueOrNull
    get() = valueOrElse { null }

val <T> Optional<T>.valueOrThrow
    get() = valueOrElse { throw IllegalStateException("Optional is None") }
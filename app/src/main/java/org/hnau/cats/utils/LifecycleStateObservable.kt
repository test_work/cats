package org.hnau.cats.utils

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


enum class LifecycleState {

    NONE, CREATED, STARTED, RESUMED

}

val Lifecycle.state: Observable<LifecycleState>
    get() = BehaviorSubject.create<LifecycleState>().apply {

        val onChanged = {
            val currentState = when (currentState) {
                Lifecycle.State.DESTROYED,
                Lifecycle.State.INITIALIZED -> LifecycleState.NONE
                Lifecycle.State.CREATED -> LifecycleState.CREATED
                Lifecycle.State.STARTED -> LifecycleState.STARTED
                Lifecycle.State.RESUMED -> LifecycleState.RESUMED
            }
            ::actualValue.setUnique(currentState)
        }

        onChanged()

        addObserver(LifecycleEventObserver { _, _ -> onChanged() })

    }

fun Observable<LifecycleState>.checkIs(
    expectedState: LifecycleState
): Observable<Boolean> = map { currentState ->
    currentState >= expectedState
}.distinctUntilChanged()

val Observable<LifecycleState>.isCreated
    get() = checkIs(LifecycleState.CREATED)
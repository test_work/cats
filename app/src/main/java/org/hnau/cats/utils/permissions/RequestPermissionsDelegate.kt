package org.hnau.cats.utils.permissions

import android.os.Build
import androidx.annotation.RequiresApi
import io.reactivex.Completable
import org.hnau.cats.utils.AndroidUtils
import org.hnau.cats.utils.AutoKeyStorage
import org.hnau.cats.utils.optional.Optional
import org.hnau.cats.utils.optional.checkOptional
import org.hnau.cats.utils.optional.valueOrNull


class RequestPermissionsDelegate(
    private val requestPermissionsContext: RequestPermissionsContext
) : PermissionsRequester {

    private val activity = requestPermissionsContext.activity

    private val callbacks = AutoKeyStorage<Int, (Optional<Boolean>) -> Unit>(
        generateKey = AndroidUtils::generateId,
        attemptsCount = 100
    )

    fun clear() = callbacks.keys.forEach { key ->
        callbacks
            .remove(key)
            .checkOptional(
                ifSome = { callback ->
                    callback(Optional.None)
                },
                ifNone = {}
            )
    }

    override fun requestPermission(
        permissionToRequest: PermissionToRequest
    ) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        requestPermissionAfterM(permissionToRequest)
    } else {
        Completable.complete()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestPermissionAfterM(
        permissionToRequest: PermissionToRequest
    ): Completable {

        val granted = PermissionsUtils.checkPermissionState(
            activity = activity,
            permissionAndroidName = permissionToRequest.androidName
        )

        if (granted) return Completable.complete()

        val showExplanationCompletable = if (
            PermissionsUtils.checkNeedShowExplanation(
                activity = activity,
                permissionAndroidName = permissionToRequest.androidName
            )
        ) {
            requestPermissionsContext.showPermissionsExplanation(permissionToRequest)
        } else {
            Completable.complete()
        }

        val requestPermissionImmediateCompletable =
            RequestPermissionsDelegateUtils
                .createRequestPermissionImmediateCompletable(
                    callbacks = callbacks,
                    permissionToRequest = permissionToRequest,
                    activity = activity
                )
                .doOnError { th ->
                    if (th is PermissionDeniedException && th.forever) {
                        requestPermissionsContext.showGoToSettingsDialog()
                    }
                }

        return showExplanationCompletable.andThen(requestPermissionImmediateCompletable)
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>
    ) {
        val permissionAndroidName = permissions.getOrNull(0) ?: return
        val callback = callbacks.remove(requestCode).valueOrNull ?: return
        callback(
            Optional.Some(
                PermissionsUtils.checkPermissionState(
                    activity = activity,
                    permissionAndroidName = permissionAndroidName
                )
            )
        )
    }

}
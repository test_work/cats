package org.hnau.cats.utils.recyclerview

import androidx.recyclerview.widget.DiffUtil


class ItemsContainer<T>(
    itemCallback: DiffUtil.ItemCallback<T>
) {

    private val diffCalculator =
        itemCallback.toDiffCalculator()

    var items: List<T> = emptyList()
        private set

    fun updateItems(
        newItems: List<T>,
        detectMoves: Boolean = true
    ): DiffUtil.DiffResult {
        val oldItems = items
        val result = diffCalculator.calcDiff(oldItems, newItems, detectMoves)
        items = newItems
        return result
    }

}
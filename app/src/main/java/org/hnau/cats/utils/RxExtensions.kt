package org.hnau.cats.utils

import android.annotation.SuppressLint
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject


@Suppress("FunctionName")
fun <T> BehaviorSubject(initialValue: T): BehaviorSubject<T> =
    BehaviorSubject.createDefault(initialValue)

@Suppress("UNCHECKED_CAST")
var <T> BehaviorSubject<T>.actualValue
    get() = value as T
    set(value) = onNext(value)

fun <T> Observable<Observable<T>>.collapse(): Observable<T> =
    switchMap { it }

fun <T> Observable<T>.useWhen(
    sign: Observable<Boolean>,
    placeholder: Observable<T> = Observable.empty()
): Observable<T> = sign.switchMap { active ->
    if (active) {
        this@useWhen
    } else {
        placeholder
    }
}

@SuppressLint("CheckResult")
inline fun <T> Observable<T>.observe(
    crossinline observer: (T) -> Unit
) {
    subscribe { value ->
        observer(value)
    }
}
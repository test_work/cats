package org.hnau.cats.utils

import org.hnau.cats.utils.optional.Optional


class AutoKeyStorage<K, V>(
    private val generateKey: () -> K,
    private val attemptsCount: Int = 100
) {

    private val storage = HashMap<K, V>()

    val keys: Set<K> by storage::keys

    fun remove(
        key: K
    ) = if (storage.containsKey(key)) {
        @Suppress("UNCHECKED_CAST")
        Optional.Some(storage.remove(key) as V)
    } else {
        Optional.None
    }

    fun insert(
        value: V
    ): Optional<K> {
        repeat(attemptsCount) {
            val key = generateKey()
            if (!storage.containsKey(key)) {
                storage[key] = value
                return Optional.Some(key)
            }
        }
        return Optional.None
    }

}
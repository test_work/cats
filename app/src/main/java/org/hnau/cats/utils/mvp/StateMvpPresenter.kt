package org.hnau.cats.utils.mvp

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import org.hnau.cats.utils.BehaviorSubject
import org.hnau.cats.utils.actualValue
import org.hnau.cats.utils.observe
import org.hnau.cats.utils.optional.Optional
import org.hnau.cats.utils.optional.checkOptional
import org.hnau.cats.utils.useWhen


abstract class StateMvpPresenter<V> : MvpPresenter<V> {

    private val viewObservableInner =
        BehaviorSubject<Optional<V>>(Optional.None)

    protected val viewObservable: Observable<Optional<V>> by ::viewObservableInner

    protected val view: Optional<V>
        get() = viewObservableInner.actualValue

    protected val isActive: Observable<Boolean> =
        viewObservable.map { view -> view is Optional.Some }

    override fun onAttach(view: V) {
        viewObservableInner.actualValue = Optional.Some(view)
    }

    override fun onDetach() {
        viewObservableInner.actualValue = Optional.None
    }

    protected inline fun withView(
        block: V.() -> Unit
    ) = view.checkOptional(
        ifSome = { view -> view.block() },
        ifNone = {}
    )

    protected inline fun <T> bindObservableToView(
        observable: Observable<T>,
        crossinline set: V.(T) -> Unit
    ) = observable
        .observeOn(AndroidSchedulers.mainThread())
        .useWhen(isActive)
        .observe { value -> withView { set(value) } }


}
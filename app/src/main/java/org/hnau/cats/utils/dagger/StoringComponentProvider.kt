package org.hnau.cats.utils.dagger

import org.hnau.cats.utils.optional.Optional


abstract class StoringComponentProvider<C : Any> : ComponentProvider<C> {

    override var component: Optional<C> = Optional.None

    protected fun setComponent(
        component: C
    ) {
        this.component = Optional.Some(component)
    }

    protected fun clearComponent() {
        this.component = Optional.None
    }


}
package org.hnau.cats.app.di.modules

import dagger.Module
import dagger.Provides
import org.hnau.cats.domain.interactors.getallcats.GetAllCatsInteractor
import org.hnau.cats.domain.interactors.getallcats.GetAllCatsInteractorImpl
import org.hnau.cats.domain.interactors.getfavoritecats.GetFavoriteCatsInteractor
import org.hnau.cats.domain.interactors.getfavoritecats.GetFavoriteCatsInteractorImpl
import org.hnau.cats.domain.interactors.managefavoritecats.ManageFavoriteCatsInteractor
import org.hnau.cats.domain.interactors.managefavoritecats.ManageFavoriteCatsInteractorImpl
import javax.inject.Singleton


@Module
class InteractorsModule {

    @Singleton
    @Provides
    fun getAllCatsInteractor(
        allCatsInteractorImpl: GetAllCatsInteractorImpl
    ): GetAllCatsInteractor = allCatsInteractorImpl

    @Singleton
    @Provides
    fun getFavoriteCatsInteractor(
        favoriteCatsInteractorImpl: GetFavoriteCatsInteractorImpl
    ): GetFavoriteCatsInteractor = favoriteCatsInteractorImpl

    @Singleton
    @Provides
    fun getManageFavoriteCatsInteractor(
        manageFavoriteCatsInteractorImpl: ManageFavoriteCatsInteractorImpl
    ): ManageFavoriteCatsInteractor = manageFavoriteCatsInteractorImpl

}
package org.hnau.cats.app.di.modules

import dagger.Module
import dagger.Provides
import org.hnau.cats.domain.repositories.AllCatsRepository
import org.hnau.cats.domain.repositories.FavoriteCatsRepository
import org.hnau.cats.repositories.allcats.AllCatsNetRepository
import org.hnau.cats.repositories.favoritecats.FavoriteCatsDBRepository
import javax.inject.Singleton


@Module
class RepositoriesModule {

    @Provides
    @Singleton
    fun getAllCatsRepository(
        allCatsNetRepository: AllCatsNetRepository
    ): AllCatsRepository = allCatsNetRepository

    @Provides
    @Singleton
    fun getFavoriteCatsRepository(
        favoriteCatsDBRepository: FavoriteCatsDBRepository
    ): FavoriteCatsRepository = favoriteCatsDBRepository

}
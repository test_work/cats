package org.hnau.cats.app.di

import dagger.Component
import org.hnau.cats.presentation.activity.di.AppActivityComponent
import org.hnau.cats.presentation.activity.di.modules.AppActivityModule
import org.hnau.cats.app.di.modules.AppModule
import org.hnau.cats.app.di.modules.InteractorsModule
import org.hnau.cats.app.di.modules.RepositoriesModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AppModule::class,
        InteractorsModule::class,
        RepositoriesModule::class
    ]
)
interface AppComponent {

    fun createAppActivityComponent(
        appActivityModule: AppActivityModule
    ): AppActivityComponent

}
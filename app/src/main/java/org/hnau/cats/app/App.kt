package org.hnau.cats.app

import android.app.Application
import org.hnau.cats.app.di.ComponentsManager


class App : Application() {

    init {
        ComponentsManager.init(this)
    }

}
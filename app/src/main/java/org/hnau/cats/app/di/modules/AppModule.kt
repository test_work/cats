package org.hnau.cats.app.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import org.hnau.cats.utils.Executors
import javax.inject.Singleton


@Module
class AppModule(
    private val context: Context
) {

    @Provides
    fun getContext() = context

    @Provides
    @Singleton
    fun getExecutors() = Executors(context)

}
package org.hnau.cats.app.di

import org.hnau.cats.app.App
import org.hnau.cats.app.di.modules.AppModule
import org.hnau.cats.presentation.activity.AppActivity
import org.hnau.cats.presentation.activity.di.AppActivityComponent
import org.hnau.cats.presentation.activity.di.modules.AppActivityModule
import org.hnau.cats.presentation.activity.screens.favorite.di.FavoriteScreenComponent
import org.hnau.cats.presentation.activity.screens.main.di.MainScreenComponent
import org.hnau.cats.utils.dagger.IndependentComponentProvider
import org.hnau.cats.utils.dagger.StoringComponentProvider
import org.hnau.cats.utils.optional.valueOrThrow


object ComponentsManager : StoringComponentProvider<AppComponent>() {

    fun init(
        app: App
    ) = setComponent(
        DaggerAppComponent.builder()
            .appModule(AppModule(app))
            .build()
    )

    class AppActivityComponentProvider(
        private val appComponent: () -> AppComponent
    ) : StoringComponentProvider<AppActivityComponent>() {

        fun fill(
            appActivity: AppActivity
        ) {
            setComponent(
                appComponent().createAppActivityComponent(
                    AppActivityModule(appActivity)
                )
            )
        }

        fun clear() {
            favoriteScreen.clear()
            mainScreen.clear()
            clearComponent()
        }

        class MainScreenComponentProvider(
            private val appActivityComponent: () -> AppActivityComponent
        ) : StoringComponentProvider<MainScreenComponent>(), IndependentComponentProvider {

            override fun fill() =
                setComponent(appActivityComponent().createMainScreenComponent())

            override fun clear() =
                clearComponent()

        }

        val mainScreen =
            MainScreenComponentProvider { component.valueOrThrow }

        class FavoriteScreenComponentProvider(
            private val appActivityComponent: () -> AppActivityComponent
        ) : StoringComponentProvider<FavoriteScreenComponent>(), IndependentComponentProvider {

            override fun fill() =
                setComponent(appActivityComponent().createFavoriteScreenComponent())

            override fun clear() =
                clearComponent()

        }

        val favoriteScreen =
            FavoriteScreenComponentProvider { component.valueOrThrow }

    }

    val appActivity =
        AppActivityComponentProvider { component.valueOrThrow }

}
package org.hnau.cats.domain.interactors.getfavoritecats

import io.reactivex.Observable
import org.hnau.cats.domain.data.Cat
import org.hnau.cats.domain.data.Favoritable


interface GetFavoriteCatsInteractor {

    fun getFavoriteCats(): Observable<List<Favoritable<Cat>>>

}
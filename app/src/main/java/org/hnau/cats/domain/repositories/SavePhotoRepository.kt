package org.hnau.cats.domain.repositories


interface SavePhotoRepository {

    fun savePhoto(url: String)

}
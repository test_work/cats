package org.hnau.cats.domain.interactors.savecatphoto

import org.hnau.cats.domain.data.Cat


interface SaveCatPhotoInteractor {

    fun saveCatPhoto(cat: Cat)

}
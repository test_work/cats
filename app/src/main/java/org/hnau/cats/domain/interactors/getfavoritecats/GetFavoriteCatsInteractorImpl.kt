package org.hnau.cats.domain.interactors.getfavoritecats

import io.reactivex.Observable
import org.hnau.cats.domain.data.Cat
import org.hnau.cats.domain.data.Favoritable
import org.hnau.cats.domain.repositories.FavoriteCatsRepository
import javax.inject.Inject


class GetFavoriteCatsInteractorImpl @Inject constructor(
    private val favoriteCatsRepository: FavoriteCatsRepository
) : GetFavoriteCatsInteractor {

    override fun getFavoriteCats(): Observable<List<Favoritable<Cat>>> =
        favoriteCatsRepository.getFavoriteCats().map { cats ->
            cats.map { cat ->
                Favoritable(
                    value = cat,
                    isFavorite = Observable.just(true)
                )
            }
        }

}
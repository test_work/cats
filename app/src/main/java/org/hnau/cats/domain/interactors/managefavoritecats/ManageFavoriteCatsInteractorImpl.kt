package org.hnau.cats.domain.interactors.managefavoritecats

import org.hnau.cats.domain.data.Cat
import org.hnau.cats.domain.repositories.FavoriteCatsRepository
import javax.inject.Inject


class ManageFavoriteCatsInteractorImpl @Inject constructor(
    private val favoriteCatsRepository: FavoriteCatsRepository
) : ManageFavoriteCatsInteractor {

    override fun addCatToFavorites(cat: Cat) =
        favoriteCatsRepository.addCatToFavorites(cat)

    override fun removeCatFromFavorites(cat: Cat) =
        favoriteCatsRepository.removeCatFromFavorites(cat.id)

}
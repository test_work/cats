package org.hnau.cats.domain.repositories

import io.reactivex.Observable
import org.hnau.cats.domain.data.Cat
import org.hnau.cats.utils.deferredloader.DeferredLoader


interface AllCatsRepository {

    fun getAllCats(): DeferredLoader<Cat>

    val isError: Observable<Boolean>

    fun reloadAll()

    fun reloadAfterError()

}
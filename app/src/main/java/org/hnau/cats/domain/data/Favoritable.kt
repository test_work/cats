package org.hnau.cats.domain.data

import io.reactivex.Observable


data class Favoritable<T>(
        val value: T,
        val isFavorite: Observable<Boolean>
)
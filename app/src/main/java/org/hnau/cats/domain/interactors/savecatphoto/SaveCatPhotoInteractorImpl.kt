package org.hnau.cats.domain.interactors.savecatphoto

import org.hnau.cats.domain.data.Cat
import org.hnau.cats.domain.repositories.SavePhotoRepository
import javax.inject.Inject


class SaveCatPhotoInteractorImpl @Inject constructor(
    private val savePhotoRepository: SavePhotoRepository
) : SaveCatPhotoInteractor {

    override fun saveCatPhoto(cat: Cat) =
        savePhotoRepository.savePhoto(cat.photoUrl)

}
package org.hnau.cats.domain.data


data class Cat(
        val id: CatId,
        val photoUrl: String
)
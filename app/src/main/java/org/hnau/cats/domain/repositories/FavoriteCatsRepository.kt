package org.hnau.cats.domain.repositories

import io.reactivex.Observable
import org.hnau.cats.domain.data.Cat
import org.hnau.cats.domain.data.CatId


interface FavoriteCatsRepository {

    fun getFavoriteCats(): Observable<List<Cat>>

    fun addCatToFavorites(cat: Cat)

    fun removeCatFromFavorites(catId: CatId)

}
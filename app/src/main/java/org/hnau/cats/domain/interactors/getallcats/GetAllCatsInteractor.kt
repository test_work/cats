package org.hnau.cats.domain.interactors.getallcats

import io.reactivex.Observable
import org.hnau.cats.domain.data.Cat
import org.hnau.cats.domain.data.Favoritable
import org.hnau.cats.utils.deferredloader.DeferredLoader


interface GetAllCatsInteractor {

    data class AllCats(
        val list: DeferredLoader<Favoritable<Cat>>,
        val isError: Observable<Boolean>
    )

    fun getAllCats(): AllCats

    fun reloadAll()

    fun reloadAfterError()

}
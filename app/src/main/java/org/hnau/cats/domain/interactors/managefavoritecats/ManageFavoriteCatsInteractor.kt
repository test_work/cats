package org.hnau.cats.domain.interactors.managefavoritecats

import org.hnau.cats.domain.data.Cat


interface ManageFavoriteCatsInteractor {

    fun addCatToFavorites(cat: Cat)

    fun removeCatFromFavorites(cat: Cat)

}
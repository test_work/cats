package org.hnau.cats.domain.interactors.getallcats

import io.reactivex.Observable
import org.hnau.cats.domain.data.Cat
import org.hnau.cats.domain.data.CatId
import org.hnau.cats.domain.data.Favoritable
import org.hnau.cats.domain.repositories.AllCatsRepository
import org.hnau.cats.domain.repositories.FavoriteCatsRepository
import org.hnau.cats.utils.deferredloader.map
import javax.inject.Inject


class GetAllCatsInteractorImpl @Inject constructor(
    private val allCatsRepository: AllCatsRepository,
    private val favoriteCatsRepository: FavoriteCatsRepository
) : GetAllCatsInteractor {

    override fun getAllCats() = GetAllCatsInteractor.AllCats(
        list = run {
            val favoriteCatsIds: Observable<HashSet<CatId>> =
                favoriteCatsRepository.getFavoriteCats().map { favoriteCats ->
                    favoriteCats.map(Cat::id).toHashSet()
                }
            allCatsRepository.getAllCats()
                .map { cat ->
                    Favoritable(
                        value = cat,
                        isFavorite = favoriteCatsIds
                            .map { favoriteCatsIdsSet ->
                                cat.id in favoriteCatsIdsSet
                            }
                            .distinctUntilChanged()
                    )
                }
        },
        isError = allCatsRepository.isError
    )

    override fun reloadAll() =
        allCatsRepository.reloadAll()

    override fun reloadAfterError() =
        allCatsRepository.reloadAfterError()

}
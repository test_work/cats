package org.hnau.cats.presentation.activity.screens.common

import android.os.Bundle
import androidx.fragment.app.Fragment
import org.hnau.cats.presentation.activity.AppScreenType
import org.hnau.cats.utils.dagger.IndependentComponentProvider
import org.hnau.cats.utils.mvp.MvpPresenter
import javax.inject.Inject


abstract class ScreenFragment<V, P : MvpPresenter<V>>(
    private val componentProvider: IndependentComponentProvider
) : Fragment() {

    abstract val screenType: AppScreenType

    protected abstract val view: V

    @Inject
    lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        componentProvider.fill()
        super.onCreate(savedInstanceState)
        injectMembers()
    }

    override fun onStart() {
        presenter.onAttach(this@ScreenFragment.view)
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
        presenter.onDetach()
    }

    override fun onDestroy() {
        super.onDestroy()
        componentProvider.clear()
    }

    protected abstract fun injectMembers()


}
package org.hnau.cats.presentation.activity.screens.favorite

import org.hnau.cats.domain.interactors.getfavoritecats.GetFavoriteCatsInteractor
import org.hnau.cats.domain.interactors.managefavoritecats.ManageFavoriteCatsInteractor
import org.hnau.cats.domain.interactors.savecatphoto.SaveCatPhotoInteractor
import org.hnau.cats.presentation.data.ViewCat
import org.hnau.cats.utils.mvp.StateMvpPresenter
import javax.inject.Inject


class FavoriteScreenPresenter @Inject constructor(
    private val manageFavoriteCatsInteractor: ManageFavoriteCatsInteractor,
    private val saveCatPhotoInteractor: SaveCatPhotoInteractor,
    getFavoriteCatsInteractor: GetFavoriteCatsInteractor
) : StateMvpPresenter<FavoriteScreen.View>(), FavoriteScreen.Presenter {

    init {

        bindObservableToView(getFavoriteCatsInteractor.getFavoriteCats()) { cats ->
            setCatsList(cats.map(ViewCat.Companion::fromFavoritableCat))
        }

    }

    override fun addCatToFavorites(cat: ViewCat) =
        manageFavoriteCatsInteractor.addCatToFavorites(cat.toCat())

    override fun removeCatFromFavorites(cat: ViewCat) =
        manageFavoriteCatsInteractor.removeCatFromFavorites(cat.toCat())

    override fun onSaveCatPhotoClick(cat: ViewCat) =
        saveCatPhotoInteractor.saveCatPhoto(cat.toCat())

}
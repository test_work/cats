package org.hnau.cats.presentation.activity.screens.main.di

import dagger.Subcomponent
import org.hnau.cats.presentation.activity.screens.main.MainScreenFragment


@MainScreenScope
@Subcomponent(
    modules = [MainScreenModule::class]
)
interface MainScreenComponent {

    fun inject(mainScreenFragment: MainScreenFragment)

}
package org.hnau.cats.presentation.activity.screens.favorite.list

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.hnau.cats.presentation.activity.screens.common.list.ViewCatDiffUtilCallback
import org.hnau.cats.presentation.activity.screens.common.list.ViewCatViewHolder
import org.hnau.cats.presentation.data.ViewCat
import org.hnau.cats.utils.recyclerview.ItemsContainer


class DiffViewCatListAdapter(
    private val callback: ViewCatViewHolder.Callback
) : RecyclerView.Adapter<ViewCatViewHolder>() {

    private val itemsContainer =
        ItemsContainer(ViewCatDiffUtilCallback)

    fun setNewItems(
        newItems: List<ViewCat>
    ) {
        val diffResult = itemsContainer.updateItems(newItems)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewCatViewHolder(parent.context, callback)

    override fun onBindViewHolder(holder: ViewCatViewHolder, position: Int) {
        val item = itemsContainer.items[position]
        holder.setItem(item)
    }

    override fun getItemCount() =
        itemsContainer.items.size

}
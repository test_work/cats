package org.hnau.cats.presentation.activity.di.modules

import dagger.Module
import dagger.Provides
import org.hnau.cats.presentation.activity.di.AppActivityScope
import org.hnau.cats.domain.repositories.SavePhotoRepository
import org.hnau.cats.repositories.savephoto.SavePhotoToDownloadsRepository


@Module
class RepositoriesModule {

    @AppActivityScope
    @Provides
    fun getSavePhotoRepository(
        savePhotoToDownloadsRepository: SavePhotoToDownloadsRepository
    ): SavePhotoRepository = savePhotoToDownloadsRepository

}
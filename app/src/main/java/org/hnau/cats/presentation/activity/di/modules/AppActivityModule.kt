package org.hnau.cats.presentation.activity.di.modules

import dagger.Module
import dagger.Provides
import org.hnau.cats.presentation.activity.AppActivity


@Module
class AppActivityModule(
    private val appActivity: AppActivity
) {

    @Provides
    fun getNavigator() =
        appActivity.navigator

    @Provides
    fun getPermissionsRequester() =
        appActivity.permissionsRequester

}
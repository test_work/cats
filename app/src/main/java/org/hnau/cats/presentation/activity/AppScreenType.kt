package org.hnau.cats.presentation.activity


enum class AppScreenType {

    MAIN,
    FAVORITE

}
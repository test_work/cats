package org.hnau.cats.presentation.activity.screens.favorite.di

import dagger.Module
import dagger.Provides
import org.hnau.cats.presentation.activity.screens.favorite.FavoriteScreen
import org.hnau.cats.presentation.activity.screens.favorite.FavoriteScreenPresenter


@Module
class FavoriteScreenModule {

    @FavoriteScreenScope
    @Provides
    fun getFavoriteScreenPresenter(
        mainScreenPresenter: FavoriteScreenPresenter
    ): FavoriteScreen.Presenter = mainScreenPresenter

}
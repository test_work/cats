package org.hnau.cats.presentation.activity.di.modules

import dagger.Module
import dagger.Provides
import org.hnau.cats.presentation.activity.di.AppActivityScope
import org.hnau.cats.domain.interactors.savecatphoto.SaveCatPhotoInteractor
import org.hnau.cats.domain.interactors.savecatphoto.SaveCatPhotoInteractorImpl


@Module
class InteractorsModule {

    @AppActivityScope
    @Provides
    fun getSaveCatPhotoInteractor(
        saveCatPhotoInteractorImpl: SaveCatPhotoInteractorImpl
    ): SaveCatPhotoInteractor = saveCatPhotoInteractorImpl

}
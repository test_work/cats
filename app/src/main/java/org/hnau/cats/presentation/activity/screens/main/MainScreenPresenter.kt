package org.hnau.cats.presentation.activity.screens.main

import org.hnau.cats.domain.interactors.getallcats.GetAllCatsInteractor
import org.hnau.cats.domain.interactors.managefavoritecats.ManageFavoriteCatsInteractor
import org.hnau.cats.domain.interactors.savecatphoto.SaveCatPhotoInteractor
import org.hnau.cats.presentation.activity.Navigator
import org.hnau.cats.presentation.data.ViewCat
import org.hnau.cats.utils.mvp.StateMvpPresenter
import javax.inject.Inject


class MainScreenPresenter @Inject constructor(
    private val navigator: Navigator,
    private val manageFavoriteCatsInteractor: ManageFavoriteCatsInteractor,
    private val saveCatPhotoInteractor: SaveCatPhotoInteractor,
    private val getAllCatsInteractor: GetAllCatsInteractor
) : StateMvpPresenter<MainScreen.View>(), MainScreen.Presenter {

    private val allCats = getAllCatsInteractor.getAllCats()

    init {

        bindObservableToView(allCats.list.items) { cats ->
            setCatsList(cats.map(ViewCat.Companion::fromFavoritableCat))
        }

        bindObservableToView(
            observable = allCats.isError,
            set = MainScreen.View::setIsError
        )

    }

    override fun onGoToFavoriteClick() =
        navigator.showFavoriteList()

    override fun addCatToFavorites(cat: ViewCat) =
        manageFavoriteCatsInteractor.addCatToFavorites(cat.toCat())

    override fun removeCatFromFavorites(cat: ViewCat) =
        manageFavoriteCatsInteractor.removeCatFromFavorites(cat.toCat())

    override fun onSaveCatPhotoClick(cat: ViewCat) {
        saveCatPhotoInteractor.saveCatPhoto(cat.toCat())
    }

    override fun onReloadAllClick() {
        getAllCatsInteractor.reloadAll()
    }

    override fun onReloadAfterErrorClick() {
        getAllCatsInteractor.reloadAfterError()
    }

    override fun onCatsListItemWasShown(itemIndex: Int) =
        allCats.list.onItemWasUsed(itemIndex)

}
package org.hnau.cats.presentation.activity.screens.favorite.di

import dagger.Subcomponent
import org.hnau.cats.presentation.activity.screens.favorite.FavoriteScreenFragment


@FavoriteScreenScope
@Subcomponent(
    modules = [FavoriteScreenModule::class]
)
interface FavoriteScreenComponent {

    fun inject(favoriteScreenFragment: FavoriteScreenFragment)

}
package org.hnau.cats.presentation.activity.dialog

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import org.hnau.cats.R
import org.hnau.cats.utils.CompletableDialogFragment
import org.hnau.cats.utils.permissions.PermissionToRequest


class PermissionExplanationDialog : CompletableDialogFragment() {

    companion object {

        private const val PERMISSION_TO_REQUEST_KEY = "permission_to_request"

        fun newInstance(
            permissionToRequest: PermissionToRequest
        ) = PermissionExplanationDialog().apply {
            arguments = Bundle().apply {
                putParcelable(PERMISSION_TO_REQUEST_KEY, permissionToRequest)
            }
        }

    }

    private val permissionToRequest: PermissionToRequest
            by lazy { arguments!!.getParcelable(PERMISSION_TO_REQUEST_KEY)!! }

    override fun onCreateDialog(
        savedInstanceState: Bundle?
    ) = AlertDialog.Builder(context!!)
        .setTitle(R.string.permission_explanation_title)
        .setMessage(permissionToRequest.descriptionResId)
        .setPositiveButton(R.string.permission_explanation_button) { _, _ -> }
        .create()

}
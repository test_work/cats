package org.hnau.cats.presentation.activity.screens.main.di

import dagger.Module
import dagger.Provides
import org.hnau.cats.presentation.activity.screens.main.MainScreen
import org.hnau.cats.presentation.activity.screens.main.MainScreenPresenter


@Module
class MainScreenModule {

    @MainScreenScope
    @Provides
    fun getMainScreenPresenter(
        mainScreenPresenter: MainScreenPresenter
    ): MainScreen.Presenter = mainScreenPresenter

}
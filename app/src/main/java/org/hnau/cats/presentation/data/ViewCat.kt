package org.hnau.cats.presentation.data

import io.reactivex.Observable
import org.hnau.cats.domain.data.Cat
import org.hnau.cats.domain.data.Favoritable


data class ViewCat(
    val id: String,
    val photoUrl: String,
    val isFavorite: Observable<Boolean>
) {

    companion object {

        fun fromFavoritableCat(
            favoritableCat: Favoritable<Cat>
        ) = ViewCat(
            id = favoritableCat.value.id,
            photoUrl = favoritableCat.value.photoUrl,
            isFavorite = favoritableCat.isFavorite
        )

    }

    fun toCat() = Cat(
        id = id,
        photoUrl = photoUrl
    )

}
package org.hnau.cats.presentation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.hnau.cats.R
import org.hnau.cats.app.di.ComponentsManager
import org.hnau.cats.presentation.activity.dialog.GoToPermissionSettingsDialog
import org.hnau.cats.presentation.activity.dialog.PermissionExplanationDialog
import org.hnau.cats.utils.permissions.PermissionsRequester
import org.hnau.cats.utils.permissions.RequestPermissionsContext
import org.hnau.cats.utils.permissions.RequestPermissionsDelegate


class AppActivity : AppCompatActivity() {

    private lateinit var screensStackDelegate: AppActivityScreensStackDelegate

    val navigator
        get() = screensStackDelegate.navigator

    private val requestPermissionsDelegate = RequestPermissionsDelegate(
        RequestPermissionsContext(
            activity = this,
            showPermissionsExplanation = { permissionToRequest ->
                PermissionExplanationDialog
                    .newInstance(permissionToRequest)
                    .apply { show(supportFragmentManager, null) }
                    .finished
            },
            showGoToSettingsDialog = {
                GoToPermissionSettingsDialog().show(supportFragmentManager, null)
            }
        )
    )

    val permissionsRequester: PermissionsRequester by ::requestPermissionsDelegate

    private val actionBar
        get() = supportActionBar!!

    override fun onCreate(savedInstanceState: Bundle?) {

        ComponentsManager.appActivity.fill(this)

        screensStackDelegate = AppActivityScreensStackDelegate(
            fragmentManager = supportFragmentManager,
            containerId = R.id.container
        )

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_app)

        AppActivityTitleDelegate(
            currentScreen = screensStackDelegate.currentScreen,
            actionBar = actionBar
        )

        supportFragmentManager
            .addOnBackStackChangedListener { updateBackButtonVisibility() }

    }

    private fun updateBackButtonVisibility() = actionBar.setDisplayHomeAsUpEnabled(
        supportFragmentManager.backStackEntryCount > 0
    )

    override fun onSupportNavigateUp(): Boolean {
        supportFragmentManager.popBackStack()
        return true
    }

    override fun onStart() {
        super.onStart()
        screensStackDelegate.onStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        ComponentsManager.appActivity.clear()
        requestPermissionsDelegate.clear()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        requestPermissionsDelegate.onRequestPermissionsResult(requestCode, permissions)
    }

}
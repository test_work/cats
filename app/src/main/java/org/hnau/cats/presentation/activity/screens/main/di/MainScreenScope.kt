package org.hnau.cats.presentation.activity.screens.main.di

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class MainScreenScope
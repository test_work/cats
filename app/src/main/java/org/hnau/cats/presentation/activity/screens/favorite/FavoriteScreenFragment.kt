package org.hnau.cats.presentation.activity.screens.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.hnau.cats.R
import org.hnau.cats.app.di.ComponentsManager
import org.hnau.cats.presentation.activity.AppScreenType
import org.hnau.cats.presentation.activity.screens.common.ScreenFragment
import org.hnau.cats.presentation.activity.screens.common.list.ViewCatViewHolder
import org.hnau.cats.presentation.activity.screens.favorite.list.DiffViewCatListAdapter
import org.hnau.cats.presentation.data.ViewCat
import org.hnau.cats.utils.optional.valueOrThrow
import org.hnau.cats.utils.recyclerview.ColumnsLayoutManager


class FavoriteScreenFragment : ScreenFragment<FavoriteScreen.View, FavoriteScreen.Presenter>(
    componentProvider = ComponentsManager.appActivity.favoriteScreen
), FavoriteScreen.View {

    override val screenType = AppScreenType.FAVORITE

    override val view: FavoriteScreen.View
        get() = this

    private lateinit var newCatsListReceiver: (List<ViewCat>) -> Unit

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(
        R.layout.fragment_favorites,
        container,
        false
    ).also { view ->

        (view as RecyclerView).apply {
            adapter = DiffViewCatListAdapter(
                callback = ViewCatViewHolder.Callback(
                    savePhoto = presenter::onSaveCatPhotoClick,
                    addCatToFavorites = presenter::addCatToFavorites,
                    removeCatFromFavorites = presenter::removeCatFromFavorites
                )
            ).also { adapter ->
                newCatsListReceiver = adapter::setNewItems
            }
            layoutManager = ColumnsLayoutManager(context)
        }

    }

    override fun setCatsList(cats: List<ViewCat>) =
        newCatsListReceiver(cats)

    override fun injectMembers() {
        ComponentsManager.appActivity.favoriteScreen.component.valueOrThrow.inject(this)
    }

}
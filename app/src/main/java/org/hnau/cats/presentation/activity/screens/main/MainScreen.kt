package org.hnau.cats.presentation.activity.screens.main

import org.hnau.cats.presentation.data.ViewCat
import org.hnau.cats.utils.mvp.MvpPresenter


interface MainScreen {

    interface Presenter : MvpPresenter<View> {

        fun onGoToFavoriteClick()

        fun addCatToFavorites(cat: ViewCat)

        fun removeCatFromFavorites(cat: ViewCat)

        fun onSaveCatPhotoClick(cat: ViewCat)

        fun onReloadAllClick()

        fun onReloadAfterErrorClick()

        fun onCatsListItemWasShown(itemIndex: Int)

    }

    interface View {

        fun setCatsList(
            cats: List<ViewCat>
        )

        fun setIsError(
            isError: Boolean
        )

    }

}
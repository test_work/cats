package org.hnau.cats.presentation.activity.dialog

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import org.hnau.cats.R


class GoToPermissionSettingsDialog : DialogFragment() {

    override fun onCreateDialog(
        savedInstanceState: Bundle?
    ) = context!!.let { context ->
        AlertDialog.Builder(context)
            .setTitle(R.string.permission_request_go_to_settings_dialog_title)
            .setMessage(R.string.permission_request_go_to_settings_dialog_text)
            .setPositiveButton(R.string.permission_request_go_to_settings_dialog_button) { _, _ ->
                activity?.startActivity(
                    Intent().apply {
                        action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        data = Uri.fromParts(
                            "package",
                            context.packageName,
                            null
                        )
                    }
                )
            }
            .create()
    }

}
package org.hnau.cats.presentation.activity.screens.common.list

import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import io.reactivex.Observable
import org.hnau.cats.R
import org.hnau.cats.presentation.data.ViewCat
import org.hnau.cats.utils.*
import org.hnau.cats.utils.optional.Optional
import org.hnau.cats.utils.optional.map
import org.hnau.cats.utils.optional.valueOrElse
import org.hnau.cats.utils.view.lifecycleState


class ViewCatViewHolder(
    private val context: Context,
    private val callback: Callback
) : RecyclerView.ViewHolder(
    context.inflateView(R.layout.item_cat)
) {

    data class Callback(
        val addCatToFavorites: (ViewCat) -> Unit,
        val removeCatFromFavorites: (ViewCat) -> Unit,
        val savePhoto: (ViewCat) -> Unit
    )

    private val catPhotoView =
        itemView.findViewById<ImageView>(R.id.image_cat)

    private val saveButton =
        itemView.findViewById<View>(R.id.button_save)

    private val toggleIsFavoriteButton =
        itemView.findViewById<ImageView>(R.id.button_toggle_is_favorite)

    private val isActive =
        itemView.lifecycleState.isCreated

    private val currentItemOptional =
        BehaviorSubject<Optional<ViewCat>>(Optional.None)

    init {
        currentItemOptional
            .map { optionalFavoritable ->
                optionalFavoritable
                    .map { cat ->
                        cat.isFavorite.map { isFavoriteValue ->
                            cat to isFavoriteValue
                        }
                    }
                    .valueOrElse { Observable.never() }
            }
            .collapse()
            .useWhen(isActive)
            .observe { (cat, isFavorite) ->
                toggleIsFavoriteButton.apply {
                    setImageResource(
                        if (isFavorite) {
                            R.drawable.ic_favorite_bordered
                        } else {
                            R.drawable.ic_not_favorite_bordered
                        }
                    )
                    setOnClickListener {
                        if (isFavorite) {
                            callback.removeCatFromFavorites(cat)
                        } else {
                            callback.addCatToFavorites(cat)
                        }
                    }
                }
            }
    }

    fun setItem(item: ViewCat) {

        currentItemOptional.actualValue = Optional.Some(item)

        Glide
            .with(context)
            .load(item.photoUrl)
            .centerCrop()
            .placeholder(R.drawable.ic_placeholder)
            .into(catPhotoView)

        saveButton.setOnClickListener {
            callback.savePhoto(item)
        }

    }
}
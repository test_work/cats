package org.hnau.cats.presentation.activity.screens.main.list

import android.os.Handler
import android.view.View
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter.StateRestorationPolicy
import org.hnau.cats.R
import org.hnau.cats.presentation.activity.screens.common.list.ViewCatViewHolder
import org.hnau.cats.presentation.data.ViewCat
import org.hnau.cats.utils.inflateView
import org.hnau.cats.utils.recyclerview.ColumnsLayoutManager
import org.hnau.cats.utils.recyclerview.SingleOrEmptyAdapter


class ViewCatListRecyclerViewWrapper(
    recyclerView: RecyclerView,
    callback: Callback,
    onItemWasShown: (itemIndex: Int) -> Unit
) {

    data class Callback(
        val item: ViewCatViewHolder.Callback,
        val reloadAfterError: () -> Unit
    )

    private val pauseBeforeUpdateHandler = Handler()

    private val context = recyclerView.context

    private val catsAdapter = DeferredLoadingViewCatListAdapter(
        callback = callback.item,
        onItemWasShown = onItemWasShown
    ).apply {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    private val isLoadingAdapter =
        SingleOrEmptyAdapter { context.inflateView(R.layout.item_loading) }

    private val isErrorAdapter =
        SingleOrEmptyAdapter {
            context
                .inflateView(R.layout.item_error)
                .apply {
                    findViewById<View>(R.id.button_reload)
                        .setOnClickListener { callback.reloadAfterError() }
                }
        }

    fun setCatsList(cats: List<ViewCat>) =
        catsAdapter.setNewItems(cats)

    // Обычно изменение состояния ошибки или загрузки происходит с обновлением списка.
    // А два последовательных обновления содержимого RecyclerView приводят к ошибке.
    // Для этого тут пауза перед обновлением
    fun setIsError(isError: Boolean) {
        pauseBeforeUpdateHandler.post {
            isErrorAdapter.setItemVisibility(isError)
            isLoadingAdapter.setItemVisibility(!isError)
        }
    }


    init {

        recyclerView.layoutManager = ColumnsLayoutManager(context).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {

                override fun getSpanSize(
                    position: Int
                ) = if (position >= catsAdapter.itemCount) {
                    ColumnsLayoutManager.columnsCount
                } else {
                    1
                }

            }
        }

        recyclerView.adapter = ConcatAdapter(
            catsAdapter,
            isLoadingAdapter,
            isErrorAdapter
        )

    }

}
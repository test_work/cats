package org.hnau.cats.presentation.activity

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import org.hnau.cats.presentation.activity.screens.favorite.FavoriteScreenFragment
import org.hnau.cats.presentation.activity.screens.main.MainScreenFragment
import org.hnau.cats.utils.actualValue
import org.hnau.cats.utils.animate
import org.hnau.cats.utils.setUnique
import org.hnau.cats.utils.transaction


class AppActivityScreensStackDelegate(
    private val fragmentManager: FragmentManager,
    @IdRes private val containerId: Int
) {

    companion object {

        private val fragmentBuilders: Map<AppScreenType, () -> Fragment> =
            AppScreenType.values().associateWith { appScreenType ->
                when (appScreenType) {
                    AppScreenType.MAIN -> ::MainScreenFragment
                    AppScreenType.FAVORITE -> ::FavoriteScreenFragment
                }
            }

    }

    val navigator = object : Navigator {

        override fun showFavoriteList() =
            showScreen(AppScreenType.FAVORITE)

    }

    private val currentScreenInner =
        BehaviorSubject.create<AppScreenType>()

    val currentScreen: Observable<AppScreenType> by ::currentScreenInner

    init {
        onStackChanged()
        fragmentManager.addOnBackStackChangedListener { onStackChanged() }
    }

    fun onStart() {
        if (fragmentManager.fragments.isEmpty()) {
            showScreen(AppScreenType.MAIN, animate = false)
        }
    }

    private fun showScreen(
        type: AppScreenType,
        animate: Boolean = true
    ) {
        fragmentManager
            .transaction {
                if (animate) animate()
                replace(containerId, fragmentBuilders.getValue(type)())
                if (fragmentManager.fragments.isNotEmpty()) {
                    addToBackStack(type.name)
                }
            }
            .commit()
    }

    private fun onStackChanged() {
        currentScreenInner::actualValue.setUnique(
            (0 until fragmentManager.backStackEntryCount)
                .mapNotNull { i -> fragmentManager.getBackStackEntryAt(i).name }
                .distinct()
                .map(AppScreenType::valueOf)
                .lastOrNull()
                ?: AppScreenType.MAIN
        )
    }

}
package org.hnau.cats.presentation.activity.di

import dagger.Subcomponent
import org.hnau.cats.presentation.activity.di.modules.AppActivityModule
import org.hnau.cats.presentation.activity.di.modules.InteractorsModule
import org.hnau.cats.presentation.activity.di.modules.RepositoriesModule
import org.hnau.cats.presentation.activity.screens.favorite.di.FavoriteScreenComponent
import org.hnau.cats.presentation.activity.screens.main.di.MainScreenComponent


@AppActivityScope
@Subcomponent(
    modules = [
        RepositoriesModule::class,
        AppActivityModule::class,
        InteractorsModule::class
    ]
)
interface AppActivityComponent {

    fun createMainScreenComponent(): MainScreenComponent

    fun createFavoriteScreenComponent(): FavoriteScreenComponent

}
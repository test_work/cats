package org.hnau.cats.presentation.activity

import androidx.appcompat.app.ActionBar
import io.reactivex.Observable
import org.hnau.cats.R
import org.hnau.cats.utils.observe


class AppActivityTitleDelegate(
    currentScreen: Observable<AppScreenType>,
    actionBar: ActionBar
) {

    init {
        currentScreen
            .observe { screenType ->
                actionBar.setTitle(
                    when (screenType) {
                        AppScreenType.MAIN -> R.string.fragment_main_title
                        AppScreenType.FAVORITE -> R.string.fragment_favorites_title
                    }
                )
            }
    }

}
package org.hnau.cats.presentation.activity.screens.common.list

import androidx.recyclerview.widget.DiffUtil
import org.hnau.cats.presentation.data.ViewCat


object ViewCatDiffUtilCallback : DiffUtil.ItemCallback<ViewCat>() {

    override fun areItemsTheSame(oldItem: ViewCat, newItem: ViewCat) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: ViewCat, newItem: ViewCat) =
        true

}
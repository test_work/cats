package org.hnau.cats.presentation.activity.screens.favorite

import org.hnau.cats.presentation.data.ViewCat
import org.hnau.cats.utils.mvp.MvpPresenter


interface FavoriteScreen {

    interface Presenter : MvpPresenter<View> {

        fun addCatToFavorites(cat: ViewCat)

        fun removeCatFromFavorites(cat: ViewCat)

        fun onSaveCatPhotoClick(cat: ViewCat)

    }

    interface View {

        fun setCatsList(
            cats: List<ViewCat>
        )

    }

}
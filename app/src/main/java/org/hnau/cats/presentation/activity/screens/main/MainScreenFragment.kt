package org.hnau.cats.presentation.activity.screens.main

import android.os.Bundle
import android.view.*
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import org.hnau.cats.R
import org.hnau.cats.app.di.ComponentsManager
import org.hnau.cats.presentation.activity.AppScreenType
import org.hnau.cats.presentation.activity.screens.common.ScreenFragment
import org.hnau.cats.presentation.activity.screens.common.list.ViewCatViewHolder
import org.hnau.cats.presentation.activity.screens.main.list.ViewCatListRecyclerViewWrapper
import org.hnau.cats.presentation.data.ViewCat
import org.hnau.cats.utils.optional.valueOrThrow


class MainScreenFragment : ScreenFragment<MainScreen.View, MainScreen.Presenter>(
    componentProvider = ComponentsManager.appActivity.mainScreen
), MainScreen.View {

    companion object {

        private const val FAVORITES_OPTIONS_MENU_ITEM_ID = 1

    }

    override val screenType = AppScreenType.MAIN

    override val view: MainScreen.View
        get() = this

    init {
        setHasOptionsMenu(true)
    }

    private lateinit var catsList: ViewCatListRecyclerViewWrapper

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(
        R.layout.fragment_main,
        container,
        false
    ).also { view ->

        (view as SwipeRefreshLayout).apply {
            setOnRefreshListener {
                isRefreshing = false
                presenter.onReloadAllClick()
            }
        }

        catsList = ViewCatListRecyclerViewWrapper(
            recyclerView = view.findViewById(R.id.list_cats),
            callback = ViewCatListRecyclerViewWrapper.Callback(
                item = ViewCatViewHolder.Callback(
                    savePhoto = presenter::onSaveCatPhotoClick,
                    addCatToFavorites = presenter::addCatToFavorites,
                    removeCatFromFavorites = presenter::removeCatFromFavorites
                ),
                reloadAfterError = presenter::onReloadAfterErrorClick
            ),
            onItemWasShown = presenter::onCatsListItemWasShown
        )

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.add(
            0,
            FAVORITES_OPTIONS_MENU_ITEM_ID,
            0,
            R.string.fragment_main_options_menu_item_favorites
        ).apply {
            setIcon(R.drawable.ic_favorite)
            setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
        }
    }

    override fun onOptionsItemSelected(
        item: MenuItem
    ) = when (item.itemId) {
        FAVORITES_OPTIONS_MENU_ITEM_ID -> {
            presenter.onGoToFavoriteClick()
            true
        }
        else -> false
    }

    override fun setIsError(isError: Boolean) =
        catsList.setIsError(isError)

    override fun setCatsList(cats: List<ViewCat>) =
        catsList.setCatsList(cats)

    override fun injectMembers() {
        ComponentsManager.appActivity.mainScreen.component.valueOrThrow.inject(this)
    }

}
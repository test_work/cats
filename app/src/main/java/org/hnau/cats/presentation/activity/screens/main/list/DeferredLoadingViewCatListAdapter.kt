package org.hnau.cats.presentation.activity.screens.main.list

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.hnau.cats.presentation.activity.screens.common.list.ViewCatViewHolder
import org.hnau.cats.presentation.data.ViewCat


class DeferredLoadingViewCatListAdapter(
    private val callback: ViewCatViewHolder.Callback,
    private val onItemWasShown: (itemIndex: Int) -> Unit
) : RecyclerView.Adapter<ViewCatViewHolder>() {

    private var currentItemsSize = 0
    private var items = emptyList<ViewCat>()

    fun setNewItems(
        newItems: List<ViewCat>
    ) {
        items = newItems
        val newItemsSize = newItems.size
        when {
            newItemsSize == 0 ->
                notifyItemRangeRemoved(0, currentItemsSize)
            newItemsSize > currentItemsSize ->
                notifyItemRangeInserted(currentItemsSize, newItemsSize - currentItemsSize)
            else ->
                notifyDataSetChanged()
        }
        currentItemsSize = newItemsSize
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewCatViewHolder(parent.context, callback)

    override fun onBindViewHolder(holder: ViewCatViewHolder, position: Int) {
        val item = items[position]
        holder.setItem(item)
        onItemWasShown(position)
    }

    override fun getItemCount() =
        items.size

}